package com.chase.schools.model.api

import com.chase.schools.model.data.School
import com.chase.schools.model.data.SchoolDetails
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface APIInterface {
    @GET("resource/f9bf-2cp4.json")
    suspend fun getSchoolDetails(@Query("dbn") dbnClicked: String): Response<Array<SchoolDetails>>

    @GET("resource/s3k6-pzi2.json")
    suspend fun getSchoolsResponse(): Response<ArrayList<School>>
}
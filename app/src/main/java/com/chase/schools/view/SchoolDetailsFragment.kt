package com.chase.schools.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.chase.schools.R
import com.chase.schools.databinding.FragmentSchoolDetailsBinding
import com.chase.schools.model.data.SchoolDetails
import com.chase.schools.viewmodel.SchoolDetailsViewModel

class SchoolDetailsFragment : Fragment() {

    private lateinit var schoolDetailsViewModel: SchoolDetailsViewModel

    private var _binding: FragmentSchoolDetailsBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        schoolDetailsViewModel = ViewModelProvider(this)[SchoolDetailsViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSchoolDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.progressBar.visibility = View.VISIBLE

        schoolDetailsViewModel.schoolDetailsLiveData.observe(viewLifecycleOwner) {
            binding.progressBar.visibility = View.GONE
            mapContents(it)
        }

        arguments?.getString("dbnClicked")?.let { schoolDetailsViewModel.getSchoolDetails(it) }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun mapContents(it: SchoolDetails?) {
        if (it != null) {
            binding.titleMathAvg.visibility = View.VISIBLE
            binding.titleReadingAvg.visibility = View.VISIBLE
            binding.titleTakers.visibility = View.VISIBLE
            binding.titleWritingAvg.visibility = View.VISIBLE
            binding.schoolDetails = it
        } else {
            Toast.makeText(
                this.activity,
                this.getString(R.string.on_empty_response),
                Toast.LENGTH_LONG
            ).show()
        }
    }

}
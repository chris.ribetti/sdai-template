package com.chase.schools.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.chase.schools.model.data.School
import com.chase.schools.model.repository.SchoolRepository
import kotlinx.coroutines.launch
import okhttp3.Response

class SchoolsViewModel(private val schoolsRepository: SchoolRepository = SchoolRepository()) :
    ViewModel() {
    var schoolsLiveData = MutableLiveData<ArrayList<School>>()
    var errorLiveDate = MutableLiveData<Response>()

    fun getSchools() {
        viewModelScope.launch {
            val response = schoolsRepository.getSchoolsAPIResponse()
            if (response.code() == 200) {
                schoolsLiveData.postValue(response.body())
            } else {
                errorLiveDate.postValue(response.raw())
            }
        }
    }
}

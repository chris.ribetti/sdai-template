package com.chase.schools.model.data

data class School(
    val dbn: String,
    val school_name: String,
    val primary_address_line_1: String,
    val city: String,
    val state_code: String,
    val zip: String
)

data class SchoolDetails(
    val dbn: String,
    val school_name: String,
    val num_of_sat_test_takers: String,
    val sat_critical_reading_avg_score: String,
    val sat_math_avg_score: String,
    val sat_writing_avg_score: String
)

package com.chase.schools

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.chase.schools.model.data.School
import com.chase.schools.viewmodel.SchoolsViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner


@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class SchoolsViewModelTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    private lateinit var viewModel: SchoolsViewModel
    private lateinit var school: School

    @Test
    fun onSuccessReturnListOfSchools() {

    }

    @Test
    fun onFailureReturnError() {

    }

}
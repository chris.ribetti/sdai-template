package com.chase.schools.model.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object APIClient {
    private const val baseURL = "https://data.cityofnewyork.us/"
    private val apiClient: Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(baseURL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun <T> getInstance(tClass: Class<T>): T {
        return apiClient.create(tClass)
    }

    var apiInterface: APIInterface = getInstance(APIInterface::class.java)
}
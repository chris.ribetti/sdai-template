package com.chase.schools.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.chase.schools.model.data.SchoolDetails
import com.chase.schools.model.repository.SchoolRepository
import kotlinx.coroutines.launch

class SchoolDetailsViewModel : ViewModel() {
    var schoolDetailsLiveData = MutableLiveData<SchoolDetails?>()
    private var schoolsRepository = SchoolRepository()

    fun getSchoolDetails(dbnClicked: String) {
        viewModelScope.launch {
            val response = schoolsRepository.getSchoolDetailsAPIResponse(dbnClicked)
            if (response.isSuccessful) {
                val schoolDetails = response.body()
                if (!schoolDetails.isNullOrEmpty())
                    schoolDetailsLiveData.postValue(schoolDetails[0])
                else
                    schoolDetailsLiveData.postValue(null)
            }
        }
    }
}
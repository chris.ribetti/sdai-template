package com.chase.schools.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.chase.schools.R
import com.chase.schools.model.data.School

class SchoolsAdapter(private val itemClickListener: OnItemClickListener) :
    RecyclerView.Adapter<SchoolsViewHolder>() {

    private var schoolsList = mutableListOf<School>()

    fun setSchools(schools: List<School>) {
        this.schoolsList = schools.toMutableList()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolsViewHolder {
        val layout =
            LayoutInflater.from(parent.context).inflate(R.layout.schools_row_item, parent, false)
        return SchoolsViewHolder(layout)
    }

    override fun onBindViewHolder(holder: SchoolsViewHolder, position: Int) {
        holder.schoolName.text = schoolsList.get(position).school_name
        holder.schoolAddress.text = schoolsList.get(position).primary_address_line_1
        holder.schoolCity.text = schoolsList.get(position).city
        holder.schoolState.text = schoolsList.get(position).state_code
        holder.schoolZip.text = schoolsList.get(position).zip
        holder.bind(schoolsList[position], itemClickListener)
    }

    override fun getItemCount(): Int {
        return schoolsList.size
    }

}

class SchoolsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var schoolName = itemView.findViewById<AppCompatTextView>(R.id.schoolName)
    var schoolAddress = itemView.findViewById<AppCompatTextView>(R.id.schoolAddress)
    var schoolCity = itemView.findViewById<AppCompatTextView>(R.id.schoolCity)
    var schoolState = itemView.findViewById<AppCompatTextView>(R.id.schoolState)
    var schoolZip = itemView.findViewById<AppCompatTextView>(R.id.schoolZip)

    fun bind(school: School, clickListener: OnItemClickListener) {
        itemView.setOnClickListener {
            clickListener.onItemClicked(school)
        }
    }

}

interface OnItemClickListener {
    fun onItemClicked(school: School)
}

